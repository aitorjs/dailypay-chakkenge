import { Ballot } from '@/types';
import DATA from '../data.json'

const api = {
  ballot: {
    list: async (): Promise<Ballot[]> => {
      console.log("DATA", DATA.items)
      return DATA.items as Ballot[]
    }
  }
}

export default api;