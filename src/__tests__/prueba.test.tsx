import { render, screen } from '@testing-library/react'
import { describe, it } from 'vitest'

import React from 'react'
import Home from '../app/page'
import { ClientComponent } from '../components/ClientComponent'
import api from '../api'

describe('App', () => {
  it('Should render', async () => {
    // act
    const home = await Home()
    render(home)

    // assert
    screen.getAllByText('AWARDS 2021')
  })
})

describe('ClientComponent', () => {
  it('Should render with correct data', async () => {
    // act
    const ballots = await api.ballot.list();
    const ballotNames = ballots.map(ballot => ballot.title);
    const nominees = ballots.map(ballot => ballot.items);

    render(<ClientComponent ballots={ballots} />)

    // assert
    ballotNames.map(ballotName => screen.getAllByText(ballotName))
    nominees[0].map(nominee => screen.getAllByText(nominee.title))
  })
})