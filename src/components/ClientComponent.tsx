'use client'
import { Ballot } from '../types';
import { Nominee } from '@/types';
import React from 'react'
import { useState } from 'react';

type Props = {
  ballots: Ballot[]
}

export function ClientComponent({ ballots }: Props) {
  // const [votes, setVotes] = useState(() => [])
  const [votes, setVotes] = useState(() => new Map<Ballot['title'], Nominee>)


  const handleVote = (ballotTitle: Ballot['title'], nominee: Nominee) => {
    /* const draft = structuredClone(votes)
    draft[ballotId] = nominee
    setVotes(draft) */

    const draft = structuredClone(votes)
    draft.set(ballotTitle, nominee)
    setVotes(draft)
  }

  const handleSubmit = () => {
    const modal = document.getElementById('defaultModal') as HTMLInputElement

    const isModal = modal?.classList.contains("hidden")
    if (isModal) {
      modal?.classList.add("none")
      modal?.classList.remove("hidden")
    } else {
      modal?.classList.add("hidden");
      modal?.classList.remove("none");
    }
  }

  function NormalizeMap() {
    const map = Array.from(votes.entries()).map(([ballotTitle, nominee]) => (`${ballotTitle}:${nominee.title}`))
    return map.map(m => {
      const [key, value] = m.split(":")
      return (<p key={key}>
        <span className="font-extrabold">{key}</span> is for&nbsp;
        <span className="font-medium">{value}</span>
      </p>
      )
    })
  }

  // console.log("votes", votes)

  return (
    <>
      <section>
        {ballots.map(ballot => (
          <article className="mb-16" key={ballot.id}>
            <h2 className=" bg-gray-400 mb-8 pl-4"> {ballot.title}</h2>
            <div className="flex flex-wrap justify-center md:justify-normal gap-8">
              {ballot.items.map(nominee => (

                <div key={nominee.id} style={{ border: votes.get(ballot.title)?.id === nominee.id ? '2px solid red' : 'none' }} className="bg-blue-300 p-4 flex flex-col justify-between w-[234px] gap-4 rounded border border-black">
                  <p className="flex items-center justify-center">{nominee.title}</p>
                  <img src={nominee.photoUrL.replaceAll(' ', '')} alt={`${nominee.title}'s poster`} className="h-full object-fill flex items-center justify-center" />
                  <button className="bg-gray-500 color-white" onClick={() => handleVote(ballot.title, nominee)}>
                    Nominee
                  </button>
                </div>
              ))}
            </div>
          </article>
        ))}

        <button type="button" onClick={() => handleSubmit()} className="rounded-full bg-yellow-600 w-64 float-right text-center mt-4 p-2 mr-4 text-white mb-4">
          Submit Ballot
        </button>

      </section>

      <section id="defaultModal" tabIndex={-1} aria-hidden="true" className="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div className="relative w-full max-w-2xl max-h-full m-auto">

          <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <div className="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
              <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                Your ballot
              </h3>
              <button type="button" onClick={() => handleSubmit()} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="defaultModal">
                <svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                </svg>
                <span className="sr-only" >Close modal</span>
              </button>
            </div>

            <div className="p-6 space-y-6">
              <div className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                <NormalizeMap />
              </div>
            </div>

            <div className="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
              <button onClick={() => handleSubmit()} type="button" className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Close</button>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
