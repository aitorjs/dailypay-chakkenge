
import api from '../api'
import { ClientComponent } from '../components/ClientComponent';
import React from 'react'

export default async function Home() {
  const ballots = await api.ballot.list();

  return (
    <main>
      <h1 className="text-center capitalize font-bold mt-4 mb-8 text-black">AWARDS 2021</h1>
      <ClientComponent ballots={ballots} />
    </main >
  )
}
