import DATA from '../../data.json'

export default function handler(req: any, res: any) {
  /* const res = await fetch('https://data.mongodb-api.com/...', {
    headers: {
      'Content-Type': 'application/json',
      'API-Key': process.env.DATA_API_KEY,
    },
  })
  const data = await res.json() */

  // return NextResponse.json({ DATA })
  res.status(200).json({ DATA })
}